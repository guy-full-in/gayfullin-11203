# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from datetime import datetime
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.utils import timezone
from exam.forms import SigninForm, UploadMaterialForm
from models import Exam, Question, Consultation, Subject, Group, Student, Teacher, Queue, Material
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from reception.models import Reception
from reception.views import teacher_only, student_only


def no_auth_please(v):
    def wrapper(request, *a, **k):
        user = request.user
        if user.is_authenticated():
            return HttpResponseRedirect(reverse("index"))
        else:
            return v(request, *a, **k)

    return wrapper


@login_required(login_url=reverse_lazy("signin"))
def consult(request, num):
    exam = Exam.objects.get(pk=num)

    if request.POST:
        user = request.user

        question = Question()
        question.text = request.POST.get('question_text')
        author = Student.objects.get(user=user)
        question.author = author
        question.consultation = exam.consultation

        question.save()

        return HttpResponseRedirect(reverse("exam:consultation", kwargs={'num': num}))
    else:
        context = {}
        if Teacher.objects.filter(user=request.user).count() > 0:
            context["teacher"] = True
            context["questions"] = Question.objects.filter(consultation=exam.consultation)
        context["consultation"] = exam.consultation
        context["exam"] = exam
        return render(request, 'exam/consultation.html', context)


@no_auth_please
def signin(request):
    if request.POST:
        form = SigninForm(request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data["username"],
                password=form.cleaned_data["password"]
            )
            if user:
                login(request, user)
                if not request.POST.get('remember_me'):
                    request.session.set_expiry(0)

                if Teacher.objects.filter(user=user).count() > 0:
                    request.session["teacher"] = True
                return HttpResponseRedirect(reverse('index'))
            else:
                return HttpResponseRedirect(reverse('signin'))

        context = {"form": form}
        return render(request, "exam/signin.html", context)
    else:
        form = SigninForm()
        context = {"form": form}
        return render(request, "exam/signin.html", context)


@login_required(login_url=reverse_lazy("signin"))
def signout(request):
    logout(request)
    return HttpResponseRedirect(reverse("signin"))


@teacher_only
@login_required(login_url=reverse_lazy("signin"))
def new_exam(request):
    if request.POST:

        queue = Queue()
        if request.POST.get("switch"):
            queue.set_students(request.POST.getlist('finalQueue'))
            queue.by_students = False
        else:
            queue.set_students([])
            queue.by_students = True
        queue.save()
        print queue

        consultation = Consultation(
            location=request.POST["consultation_location"],
            start_time=request.POST["consultation_start_time"]
        )
        consultation.save()

        exam = Exam(
            teacher=Teacher.objects.get(user=request.user),
            subject=Subject.objects.get(pk=request.POST["subject"]),
            date=request.POST["date"],
            start_time=request.POST["start_time"],
            end_time=request.POST["end_time"],
            students_per_time=request.POST["students_per_time"],
            time_for_student=request.POST["time_for_student"],
            location=request.POST["location"],
            queue=queue,
            consultation=consultation,
        )

        exam.save()

        for material_file in request.FILES.getlist('file'):
            material = Material()
            material.file = material_file
            material.name = str(material_file)
            material.exam = exam
            material.save()

        for group in request.POST.getlist("groups"):
            exam.groups.add(Group.objects.get(pk=group))
        exam.save()
        request.session["last_exam"] = exam.pk
        return HttpResponseRedirect(reverse("exam:details", kwargs={'num': exam.pk}), )
    else:
        teacher = Teacher.objects.get(user=request.user)
        subjects = {}
        for subject in teacher.subject.all():
            subjects[str(subject.pk)] = subject.name

        groups = {}
        for group in teacher.groups.all():
            groups[str(group.pk)] = group.number

        context = {}
        form = UploadMaterialForm()
        context["groups"] = groups
        context["subjects"] = subjects
        context["form"] = form
        return render(request, "exam/new.html", context)


@teacher_only
@login_required(login_url=reverse_lazy("signin"))
def get_group(request, num):
    group = Group.objects.get(pk=num)
    students = Student.objects.filter(group=group)
    result = {}
    for s in students:
        result[str(s.pk)] = s.user.last_name + ' ' + s.user.first_name

    return HttpResponse(json.dumps(result), content_type="application/json")


@login_required(login_url=reverse_lazy("signin"))
def edit_exam(request, num):
    if request.POST:
        exam = Exam.objects.get(pk=num)

        if request.POST.get("switch"):
            exam.queue.set_students(request.POST.getlist('finalQueue'))
            exam.queue.by_students = False
        elif not exam.queue.by_students:
            exam.queue.set_students([])
            exam.queue.by_students = True

        consultation = Consultation.objects.get(pk=exam.consultation.pk)
        consultation.location = request.POST["consultation_location"],
        consultation.start_time = request.POST["consultation_start_time"]

        exam.consultation = consultation

        exam.teacher = Teacher.objects.get(user=request.user)
        exam.subject = Subject.objects.get(pk=request.POST["subject"])
        exam.date = request.POST["date"]
        exam.start_time = request.POST["start_time"]
        exam.end_time = request.POST["end_time"]
        exam.students_per_time = request.POST["students_per_time"]
        exam.time_for_student = request.POST["time_for_student"]
        exam.location = request.POST["location"]

        exam.save()

        for material_file in request.FILES.getlist('file'):
            material = Material()
            material.file = material_file
            material.name = str(material_file)
            material.exam = exam
            material.save()

        exam.groups.remove()
        for group in request.POST.getlist("groups"):
            exam.groups.add(Group.objects.get(pk=group))
        exam.save()
        request.session["last_exam"] = exam.pk
        return HttpResponseRedirect(reverse("exam:details", kwargs={'num': exam.pk}), )
    else:
        exam = Exam.objects.get(pk=num)
        teacher = Teacher.objects.get(user=request.user)
        subjects = {}
        for subject in teacher.subject.all():
            subjects[str(subject.pk)] = subject.name

        selected_groups = []
        for group in exam.groups.all():
            selected_groups.append(str(group.pk))
        groups = {}
        for group in teacher.groups.all():
            groups[str(group.pk)] = group.number

        context = {}
        context["exam"] = exam
        context["selected_groups"] = selected_groups
        context["groups"] = groups
        context["subjects"] = subjects
        return render(request, "exam/edit.html", context)


@login_required(login_url=reverse_lazy("signin"))
def delete_exam(request, num):
    Exam.objects.get(pk=num).delete()
    del request.session["last_exam"]
    return HttpResponseRedirect(reverse("index"))


@login_required(login_url=reverse_lazy("signin"))
def change_password(request):
    if request.POST:
        user = request.user
        context = {}
        if request.POST["new_password"] == request.POST["confirm_new_password"]:
            if request.POST["old_password"] == user.password:
                user.set_password(request.POST["new_password"])
                user.save()
            else:
                context["error"] = "Вы ввели неверный старый пароль"
                return render(request, "exam/change_password.html", context)
        else:
            context["error"] = "Новые пароли не совпадают"
            return render(request, "exam/change_password.html", context)
    else:
        return render(request, "exam/change_password.html")


@login_required(login_url=reverse_lazy("signin"))
def index(request):
    context = {}
    if Student.objects.filter(user=request.user).count() > 0:
        student = Student.objects.get(user=request.user)
        next_exams = Exam.objects.filter(
            Q(date__gte=datetime.now()) & Q(groups__id__exact=student.group.pk) & Q(start=False))
        exams = Exam.objects.filter(groups__id__exact=student.group.pk)
    else:
        teacher = Teacher.objects.get(user=request.user)
        next_exams = Exam.objects.filter(date__gte=datetime.now(), teacher=teacher, start=False)
        exams = Exam.objects.filter(teacher=teacher)
    list = []
    for exam in exams:
        list.append({'pk': exam.pk, 'name': exam.subject.name, 'date': str(exam.date)})
    context["next_exams"] = next_exams
    context["exams"] = json.dumps({'response': list})
    return render(request, "exam/index.html", context)


@login_required(login_url=reverse_lazy("signin"))
def details(request, num):
    context = {}
    if Student.objects.filter(user=request.user).count() > 0:
        context["student"] = Student.objects.get(user=request.user).pk
    exam = Exam.objects.raw("select * from exam_exam where id = %s", [num])[0]
    request.session["last_exam"] = exam.pk
    context["exam"] = exam
    materials = Material.objects.filter(exam=exam)
    context["materials"] = materials
    if timezone.now() < exam.consultation.start_time:
        context["can_ask"] = True
    queue = []
    for s in exam.queue.get_students():
        student = Student.objects.get(pk=s)
        queue.append(student.user.last_name + ' ' + student.user.first_name)
    context["queue"] = queue
    if exam.date == datetime.now().date() and exam.start_time <= datetime.now().time():
        context["can_start"] = True

    if Reception.objects.filter(exam=exam).count() > 0:
        reception = Reception.objects.get(exam=exam)
        if reception.end_time:
            context["finish"] = True
            context["can_start"] = False

    return render(request, "exam/details.html", context)


@login_required(login_url=reverse_lazy("signin"))
@student_only
def queue_by_students(request, num):
    student = Student.objects.get(user=request.user)
    exam = Exam.objects.get(pk=num)
    if request.POST:
        queue = exam.queue.get_students()
        queue.append(student.pk)
        exam.queue.set_students(queue)
        exam.queue.save()
        return HttpResponseRedirect(reverse("exam:details", kwargs={'num': exam.pk}), )
    else:
        if student and exam.queue.by_students and not student.pk in exam.queue.get_students():
            context = {}
            context["exam"] = exam.pk
            queue = {}
            for s in exam.queue.get_students():
                student = Student.objects.get(pk=s)
                queue[s] = student.user.last_name + ' ' + student.user.first_name
            context["queue"] = queue
            return render(request, "exam/queue_by_students.html", context)
        return HttpResponseRedirect(reverse("exam:details", kwargs={'num': exam.pk}), )


@login_required(login_url=reverse_lazy("signin"))
@teacher_only
def start_exam(request, num):
    exam = Exam.objects.get(pk=num)
    exam_datetime = datetime.combine(exam.date, exam.start_time)
    if exam_datetime <= datetime.now() and not exam.start:
        exam.start = True
        queue = exam.queue.get_students()
        for group in exam.groups.all():
            students = Student.objects.filter(group=group)
            for student in students:
                if not student.pk in queue:
                    queue.append(student.pk)

        exam.queue.set_students(queue)
        exam.queue.save()
        exam.save()
        return HttpResponseRedirect(reverse("reception:start", kwargs={'exam_num': exam.pk}))
    return HttpResponseRedirect(reverse("exam:details", kwargs={'num': exam.pk}))

