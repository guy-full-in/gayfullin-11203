# -*- coding: utf-8 -*-
from django import forms
from exam.models import Material


class SigninForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'type': "username", 'placeholder': 'Логин'}))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'type': 'password', 'placeholder': 'Пароль'}))


class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(widget=forms.PasswordInput)
    new_password = forms.CharField(widget=forms.PasswordInput)
    confirm_new_password = forms.CharField(widget=forms.PasswordInput)


class UploadMaterialForm(forms.Form):
    material = forms.FileField(label='Select a file', help_text='max. 42 megabytes')

    class Meta:
        model = Material
