#include "mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include "limits.h"
#include "omp.h"
#include <random>
#include <iostream>


using namespace std;


int task1(int argc, char **argv) {
	int nPr, size, nPr_in_new, nPr_in_world;
	int N = 10;
	float *A = new float[N];

	MPI_Group wgroup, new_group;
	MPI_Comm NEW_COMM;
	MPI_Status status;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &nPr);

	MPI_Comm_group(MPI_COMM_WORLD, &wgroup);
	int ranks[5] = { 8, 3, 9, 1, 6 };
	MPI_Group_incl(wgroup, 5, ranks, &new_group);
	MPI_Comm_create(MPI_COMM_WORLD, new_group, &NEW_COMM);

	if (size == 10){
		if (NEW_COMM != MPI_COMM_NULL){

			MPI_Comm_rank(NEW_COMM, &nPr_in_new);
			if (nPr_in_new == 0){
				for (int i = 0; i < N; i++)
				{
					A[i] = rand() % 10;
					printf("%f ", A[i]);
				}
				printf("\n\n");
			}

			MPI_Bcast(A, N, MPI_FLOAT, 0, NEW_COMM);
			if (nPr_in_new != 0){
				for (int i = 0; i < N; i++)
				{
					A[i] = rand() % 10;
					printf("%f ", A[i]);
				}
				printf("\n\n");
			}

		}


		if (nPr == 6){
			MPI_Send(A, N, MPI_FLOAT, 0, 17, MPI_COMM_WORLD);
		}

		if (nPr == 0){
			MPI_Recv(A, N, MPI_FLOAT, 6, 17, MPI_COMM_WORLD, &status);
			for (int i = 0; i < N; i++)
			{
				A[i] = rand() % 10;
				printf("%f ", A[i]);
			}
			printf("\n\n");
		}

	}
	else
	{
		printf("Error");
	}

	MPI_Finalize();

	delete A;

	return 0;
}


int task2(int argc, char **argv) {
	int rank, size;
	int ranks[2] = { 0, 2 };

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (size == 12){

		MPI_Group wgroup, group1, group2, group3, group4;
		MPI_Comm_group(MPI_COMM_WORLD, &wgroup);
		int ranks1[3] = { 0, 1, 2 };
		MPI_Group_incl(wgroup, 3, ranks1, &group1);
		int ranks2[3] = { 3, 4, 5 };
		MPI_Group_incl(wgroup, 3, ranks2, &group2);
		int ranks3[3] = { 6, 7, 8 };
		MPI_Group_incl(wgroup, 3, ranks3, &group3);
		int ranks4[3] = { 9, 10, 11 };
		MPI_Group_incl(wgroup, 3, ranks4, &group4);

		MPI_Comm comm1, comm2, comm3, comm4, icomm1, icomm2;
		MPI_Comm_create(MPI_COMM_WORLD, group1, &comm1);
		MPI_Comm_create(MPI_COMM_WORLD, group2, &comm2);
		MPI_Comm_create(MPI_COMM_WORLD, group3, &comm3);
		MPI_Comm_create(MPI_COMM_WORLD, group4, &comm4);

		int rank1, rank2, rank3, rank4;

		if (comm1 != MPI_COMM_NULL) MPI_Comm_rank(comm1, &rank1);
		if (comm2 != MPI_COMM_NULL) MPI_Comm_rank(comm2, &rank2);
		if (comm3 != MPI_COMM_NULL) MPI_Comm_rank(comm3, &rank3);
		if (comm4 != MPI_COMM_NULL) MPI_Comm_rank(comm4, &rank4);


		int N = 3;
		int *big_buff = new int[N * 3];
		int *global_buff = new int[N * 3];
		int *buff = new int[N];
		for (int i = 0; i < N; i++)
		{
			int value;
			if (rank < 3){
				value = 1;
			}
			else if (rank<6)
			{
				value = 2;
			}
			else if (rank<9)
			{
				value = 3;
			}
			else if (rank<12)
			{
				value = 4;
			}

			buff[i] = value;
		}


		if (comm1 != MPI_COMM_NULL) MPI_Gather(buff, N, MPI_INT, big_buff, N , MPI_INT, 0, comm1);
		if (comm2 != MPI_COMM_NULL) MPI_Gather(buff, N, MPI_INT, big_buff, N , MPI_INT, 0, comm2);
		if (comm3 != MPI_COMM_NULL) MPI_Gather(buff, N, MPI_INT, big_buff, N , MPI_INT, 0, comm3);
		if (comm4 != MPI_COMM_NULL) MPI_Gather(buff, N, MPI_INT, big_buff, N , MPI_INT, 0, comm4);
		

		if (rank < 3){
			MPI_Intercomm_create(comm1, 0, MPI_COMM_WORLD,3 , 12, &icomm1);
		}
		else if (rank<6 && rank >= 3)
		{
			MPI_Intercomm_create(comm2, 0, MPI_COMM_WORLD, 0, 12, &icomm1);
		}
		else if (rank<9 && rank >=6)
		{
			MPI_Intercomm_create(comm3, 0, MPI_COMM_WORLD, 9, 34, &icomm2);
		}
		else if (rank<12 && rank >= 9)
		{
			MPI_Intercomm_create(comm4, 0, MPI_COMM_WORLD, 6, 34, &icomm2);
		}


		MPI_Status status;

		//if (rank1 == 0){
		//	MPI_Send(big_buff, N * 3, MPI_INT, 0, 11, icomm1);
		//}

		//if (rank2 == 0){
		//	MPI_Recv(global_buff, N * 3, MPI_INT, MPI_ANY_SOURCE, 11, icomm1, &status);
		//}


		if (rank1 == 0){
			MPI_Send(big_buff, N * 3, MPI_INT, 0, 11, icomm1);
		}
		else if(rank2 == 0){
			MPI_Send(big_buff, N * 3, MPI_INT, 0, 12, icomm1);
		}
		else if (rank3 == 0){
			MPI_Send(big_buff, N * 3, MPI_INT, 0, 13, icomm2);
		}
		else if (rank4 == 0){
			MPI_Send(big_buff, N * 3, MPI_INT, 0, 14, icomm2);
		}



		if (rank1 == 0){
			MPI_Recv(global_buff, N * 3, MPI_INT, MPI_ANY_SOURCE, 12, icomm1, &status);
		}
		else if (rank2 == 0){
			MPI_Recv(global_buff, N * 3, MPI_INT, MPI_ANY_SOURCE, 11, icomm1, &status);
		}
		else if (rank3 == 0){
			MPI_Recv(global_buff, N * 3, MPI_INT, MPI_ANY_SOURCE, 14, icomm2, &status);
		}
		else if (rank4 == 0){
			MPI_Recv(global_buff, N * 3, MPI_INT, MPI_ANY_SOURCE, 13, icomm2, &status);
		}

		if (rank1 == 0 || rank2 ==0 || rank3==0 || rank4==0){
			for (int i = 0; i < N*3; i++)
			{
				printf("%d ", global_buff[i]);
			}
			printf("\n\n");
		}
	}
	else
	{
		printf("Error");
	}

	MPI_Finalize();

	return 0;
}




void main(int argc, char* argv[])
{
	//task1(argc, argv);
	task2(argc, argv);
	//cin.get();
}
