from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from exam.models import *
from reception.models import Reception, ReceptionPlace


class CourseAdmin(admin.ModelAdmin):
    list_display = ('pk', 'number', 'specialty')
    list_filter = ['number', 'specialty']


class GroupAdmin(admin.ModelAdmin):
    list_display = ('pk', 'number', 'course')
    list_filter = ['number', 'course']


class StudentAdmin(admin.ModelAdmin):
    list_display = ('pk', 'group', 'user')
    list_filter = ['group', 'user']


class SubjectAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'hours')
    list_filter = ['name', 'hours']


class TeacherAdmin(admin.ModelAdmin):
    list_display = ('pk', 'get_subjects', 'get_groups', 'user')
    list_filter = ['user']


class QueueAdmin(admin.ModelAdmin):
    list_display = ('pk', 'by_students', 'get_students')
    list_filter = ['by_students']


class ConsultationAdmin(admin.ModelAdmin):
    list_display = ('pk', 'start_time', 'location')
    list_filter = ['start_time', 'location']


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('pk', 'text', 'author', 'date', 'consultation')
    list_filter = ['text', 'author', 'date', 'consultation']


class ExamAdmin(admin.ModelAdmin):
    list_display = ('pk', 'subject', 'teacher', 'get_groups', 'date', 'start_time', 'end_time', 'location', 'queue', 'students_per_time', 'time_for_student', 'consultation')
    list_filter = ['subject', 'teacher', 'date', 'start_time', 'end_time', 'location', 'queue', 'students_per_time', 'time_for_student', 'consultation']


class MaterialAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'file', 'exam')
    list_filter = ['name', 'file', 'exam']

admin.site.register(Course, CourseAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Student, StudentAdmin)
admin.site.register(Subject, SubjectAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Queue, QueueAdmin)
admin.site.register(Consultation, ConsultationAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Exam, ExamAdmin)
admin.site.register(Material, MaterialAdmin)


admin.site.unregister(User)


class MyUserAdmin(UserAdmin):
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'first_name', 'last_name', 'password1', 'password2')}
        ),
    )
    list_display = ('pk', 'username', 'first_name', 'last_name')


admin.site.register(User, MyUserAdmin)