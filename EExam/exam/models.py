# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
import json


class Course(models.Model):
    number = models.IntegerField()
    specialty = models.CharField(max_length=120, blank=True)

    def __unicode__(self):
        return str(self.number)


class Group(models.Model):
    number = models.CharField(max_length=10, blank=True)
    course = models.ForeignKey(Course)

    def __unicode__(self):
        return str(self.number)


class Student(models.Model):
    group = models.ForeignKey(Group)
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)

    def __unicode__(self):
        return self.user.first_name + ' ' + self.user.last_name


class Subject(models.Model):
    name = models.CharField(max_length=120, blank=True)
    hours = models.IntegerField()

    def __unicode__(self):
        return self.name


class Teacher(models.Model):
    subject = models.ManyToManyField(Subject)
    groups = models.ManyToManyField(Group)
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)

    def __unicode__(self):
        return self.user.first_name + ' ' + self.user.last_name

    def get_subjects(self):
        return "\n".join([s.name for s in self.subject.all()])

    def get_groups(self):
        return "\n".join([g.number for g in self.groups.all()])


class Queue(models.Model):
    by_students = models.BooleanField()
    students = models.CharField(max_length=1000)

    def set_students(self, x):
        self.students = json.dumps(x)

    def get_students(self):
        return json.loads(self.students)

    def __unicode__(self):
        return 'Очередь ' + str(self.pk)


class Consultation(models.Model):
    start_time = models.DateTimeField()
    location = models.CharField(max_length=50)

    def __unicode__(self):
        return 'Консультация ' + str(self.pk)


class Question(models.Model):
    text = models.CharField(max_length=255, blank=True)
    author = models.ForeignKey(Student)
    date = models.DateTimeField(auto_now=True)
    consultation = models.ForeignKey(Consultation)

    def __unicode__(self):
        return 'Вопрос от ' + self.author.user.first_name + ' ' + self.author.user.last_name


class Exam(models.Model):
    teacher = models.ForeignKey(Teacher)
    subject = models.ForeignKey(Subject)
    groups = models.ManyToManyField(Group)
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    queue = models.ForeignKey(Queue)
    students_per_time = models.IntegerField()
    time_for_student = models.IntegerField()
    location = models.CharField(max_length=50)
    consultation = models.ForeignKey(Consultation)
    start = models.BooleanField(default=False)

    def __unicode__(self):
        return 'Экзамен по ' + self.subject.name

    def get_groups(self):
        return "\n".join([g.number for g in self.groups.all()])


class Material(models.Model):
    name = models.CharField(max_length=50, blank=True)
    file = models.FileField(upload_to="files/")
    exam = models.ForeignKey(Exam)

    def __unicode__(self):
        return self.name