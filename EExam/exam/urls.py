from django.conf.urls import url, patterns
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic import TemplateView

urlpatterns = patterns('exam.views',
    url(r'^(?P<num>[0-9]+)/consultation/$', 'consult', name='consultation'),
    url(r'^new$', 'new_exam', name="new"),
    url(r'^(?P<num>[0-9]+)$', 'details', name="details"),
    url(r'^(?P<num>[0-9]+)/edit$', 'edit_exam', name='edit'),
    url(r'^(?P<num>[0-9]+)/delete', 'delete_exam', name='delete'),
    url(r'^(?P<num>[0-9]+)/queue$', 'queue_by_students', name="queue_by_students"),
    url(r'^(?P<num>[0-9]+)/start$', 'start_exam', name="start_exam")
)