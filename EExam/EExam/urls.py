from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.views.generic import TemplateView, DetailView
from django.contrib.auth.decorators import login_required
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.core.urlresolvers import reverse, reverse_lazy
from EExam import settings

admin.autodiscover()



urlpatterns = patterns('',
    url(r'^exam/', include('exam.urls', namespace='exam')),
    url(r'^reception/', include('reception.urls', namespace='reception')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^group/(?P<num>[0-9]+)/$', 'exam.views.get_group', name="get_group"),
    url(r'^signin/$', 'exam.views.signin', name="signin"),
    url(r'^about/$', login_required(TemplateView.as_view(template_name="exam/about.html"), login_url=reverse_lazy("signin")), name="about"),
    url(r'^change-password$', 'exam.views.change_password', name="change_password"),
    url(r'^$', 'exam.views.index', name="index"),
    url(r'^signout/$', 'exam.views.signout', name='signout'),
)


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()