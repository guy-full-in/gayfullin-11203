from django.contrib import admin
from reception.models import *


class ReceptionAdmin(admin.ModelAdmin):
    list_display = ('pk', 'exam')
    list_filter = ['exam']


class ReceptionPlaceAdmin(admin.ModelAdmin):
    list_display = ('pk', 'number', 'reception_time', 'student', 'status', 'start_time', 'end_time', 'evaluation', 'reception')
    list_filter = ['number', 'reception_time', 'student', 'status', 'start_time', 'end_time', 'evaluation', 'reception']


admin.site.register(Reception, ReceptionAdmin)
admin.site.register(ReceptionPlace, ReceptionPlaceAdmin)

