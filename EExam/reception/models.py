# -*- coding: utf-8 -*-

from django.db import models
from exam.models import Exam, Student


class Reception(models.Model):
    exam = models.ForeignKey(Exam)
    start_time = models.DateTimeField(auto_now=True)
    end_time = models.DateTimeField(null=True)

    def __unicode__(self):
        return 'Reception ' + self.exam.subject.name


class ReceptionPlace(models.Model):

    STATUS_CHOICES = (
        (0, 'В очереди'),
        (1, 'Ожидают'),
        (3, 'Сдают'),
    )

    number = models.IntegerField()
    reception_time = models.DateTimeField()  # ожидаемое время приема
    student = models.ForeignKey(Student)
    status = models.IntegerField(choices=STATUS_CHOICES, default=0)
    start_time = models.TimeField(null=True)
    end_time = models.TimeField(null=True)
    evaluation = models.IntegerField(null=True)
    reception = models.ForeignKey(Reception)

    def __unicode__(self):
        return 'ReceptionPlace ' + self.student.user.first_name + ' ' + self.student.user.last_name