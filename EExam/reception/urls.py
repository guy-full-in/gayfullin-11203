from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

from django.core.urlresolvers import reverse_lazy

urlpatterns = patterns('reception.views',
    url(r'^(?P<exam_num>[0-9]+)/teacher$', 'index_teacher', name="teacher"),
    url(r'^(?P<exam_num>[0-9]+)/student$', 'index_student', name="student"),
    url(r'^(?P<exam_num>[0-9]+)/start$', 'start_reception', name="start"),
    url(r'^(?P<exam_num>[0-9]+)/finish$', 'finish_exam', name="finish"),
    url(r'^(?P<exam_num>[0-9]+)/student/(?P<stud_num>[0-9]+)/start$', 'start_reception_for_student', name="start_student"),
    url(r'^(?P<exam_num>[0-9]+)/student/(?P<stud_num>[0-9]+)/acceptance$', 'acceptance_reception_for_student', name='acceptance_student'),
    url(r'^(?P<exam_num>[0-9]+)/student/(?P<stud_num>[0-9]+)/finish', 'finish_reception_for_student', name='finish_student'),
)
