from datetime import timedelta, datetime
import json
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from exam.models import Exam, Student, Teacher
from reception.models import Reception, ReceptionPlace


def teacher_only(v):
    def wrapper(request, *a, **k):
        user = request.user
        if Student.objects.filter(user=user).count() > 0:
            return HttpResponseRedirect(reverse("index"))
        else:
            return v(request, *a, **k)

    return wrapper


def student_only(v):
    def wrapper(request, *a, **k):
        user = request.user
        if Teacher.objects.filter(user=user).count() > 0:
            return HttpResponseRedirect(reverse("index"))
        else:
            return v(request, *a, **k)

    return wrapper


@teacher_only
def start_reception(request, exam_num):
    exam = Exam.objects.get(pk=exam_num)
    reception = Reception(
        exam=exam
    )
    reception.save()

    count = 1
    students_per_time = exam.students_per_time
    time_for_student = exam.time_for_student
    for student_pk in exam.queue.get_students():
        if students_per_time == 0:
            time_for_student += exam.time_for_student
        reception_time = reception.start_time + timedelta(minutes=time_for_student)
        student = Student.objects.get(pk=student_pk)
        reception_place = ReceptionPlace(
            number=count,
            reception_time=reception_time,
            student=student,
            status=0,
            reception=reception
        )
        reception_place.save()
        count += 1
        students_per_time -= 1

    return HttpResponseRedirect(reverse("reception:teacher", kwargs={'exam_num': exam_num}))


@teacher_only
def index_teacher(request, exam_num):
    context = {}
    exam = Exam.objects.get(pk=exam_num)
    reception = Reception.objects.get(exam=exam)
    queue = []
    for student_pk in exam.queue.get_students():
        student = Student.objects.get(pk=student_pk)
        reception_place = ReceptionPlace.objects.get(reception=reception, student=student)
        if not reception_place.end_time:
            queue.append(Student.objects.get(pk=student_pk))
    print queue
    context["queue"] = queue
    context["exam"] = exam.pk
    return render(request, "reception/teacher.html", context)


@teacher_only
def start_reception_for_student(request, exam_num, stud_num):
    exam = Exam.objects.get(pk=exam_num)
    student = Student.objects.get(pk=stud_num)
    reception = Reception.objects.get(exam=exam)
    reception_place = ReceptionPlace.objects.get(reception=reception, student=student)
    reception_place.start_time = datetime.now()
    reception_place.status = 1

    student = {
        "pk": student.pk,
        "first_name": student.user.first_name,
        "last_name": student.user.last_name,
        "group": student.group.number
    }
    data = {}
    data["student"] = student

    reception_place.save()
    return HttpResponse(json.dumps(data), content_type="application/json")


@teacher_only
def acceptance_reception_for_student(request, exam_num, stud_num):
    exam = Exam.objects.get(pk=exam_num)
    student = Student.objects.get(pk=stud_num)
    reception = Reception.objects.get(exam=exam)
    reception_place = ReceptionPlace.objects.get(reception=reception, student=student)
    reception_place.status = 3

    student = {
        "pk": student.pk,
        "first_name": student.user.first_name,
        "last_name": student.user.last_name,
        "group": student.group.number
    }
    data = {}
    data["student"] = student

    reception_place.save()
    return HttpResponse(json.dumps(data), content_type="application/json")


@teacher_only
def finish_reception_for_student(request, exam_num, stud_num):
    exam = Exam.objects.get(pk=exam_num)
    student = Student.objects.get(pk=stud_num)
    reception = Reception.objects.get(exam=exam)
    reception_place = ReceptionPlace.objects.get(reception=reception, student=student)
    reception_place.end_time = datetime.now()
    reception_place.evaluation = int(request.GET["eval"])

    reception_place.save()
    return HttpResponse(json.dumps("Ok"), content_type="application/json")


@teacher_only
def finish_exam(request, exam_num):
    exam = Exam.objects.get(pk=exam_num)
    reception = Reception.objects.get(exam=exam)
    reception.end_time = datetime.now()
    reception.save()
    return HttpResponseRedirect(reverse("exam:details", kwargs={'num': exam_num}))


@student_only
def index_student(request, exam_num):
    context = {}
    exam = Exam.objects.get(pk=exam_num)
    student = Student.objects.get(user=request.user)
    reception = Reception.objects.get(exam=exam)
    reception_place = ReceptionPlace.objects.get(reception=reception, student=student)
    context["place"] = reception_place
    return render(request, 'reception/student.html', context)
